# test-sre-j2m

This document describes the actions and thought process for this exercise.

## Initial setup

As i already had a gitlab account i decided to create a new project called **test-sre**.

I have downloaded the Github repository and after that I commited the code to my Gitlab project as it was one of the requirements.

``` Shell
~/ $ git clone https://github.com/psantori/j2m-sre-test.git
~/ $ cd j2m-sre-test/
~/j2m-sre-test/ $ git remote add gitlab git@gitlab.com:Endriuz22/test-sre.git
~/j2m-sre-test/ $ git push gitlab master

~/ $ git clone git@gitlab.com:Endriuz22/test-sre.git
~/ $ cd test-sre/
```
I moved the source files to /src folder to have a proper structure

### Continuous integration

After looking into the app source code i understood that we have a `greet()` function, when called, will return a string with an integer passed as argument.

When you call the `main()` function it first initializes a Redis Client, passing the Redis DB address from an environment variable `REDIS_URL`, then starts the Fiber web server.

Then, there is a Route defined to respond to all `GET` HTTP(S) Requests sent to the Root Path (`/`).
When "called", it will try to increment the `counter` within the Redis Database and get the incremented counter value.
In case of failure, _"the application will panic"_ -- i.e. will respond to the request with the actual error ocurred.
In case of success, the `greet()` function will be called, passing the `counter` as parameter. The function result will be sent back as string, as Request Response.

Next, the Web Server is set to listen to requests on the Address set as Environment Variable (`SERVICE_ADDR`).
In case of Web Server Failure, the error will be logged/outputted.


## Architecture

At this point I decided to build the CI/CD part on Gitlab, even though I created a docker-compose file to be used locally by devs for example.

How would the entire architecture would look like?
![Test Image 1](job2me.png)

## Docker

Good! Now let's build the Docker file.

I have choosed a golang image to build the artifacts.

```
FROM golang:1.16-buster AS build
```
In the end I build the app on another image in order to make the image much smaller.
```
FROM gcr.io/distroless/base-debian10
```
Some comments are written inside the Dockerfile.

## Gitlab

_Note: Forgot to mention that in advance i have build a Kubernetes cluster in GCP. This could have been automated as well with terraform._

I have added the Gitlab Agent into my K8s cluster as per the [documentation](https://docs.gitlab.com/ee/user/clusters/agent/install/) so that i can deploy the solution on it.

Ok. After building the Dockerfile I moved to the `.gitlab-ci.yaml`

After building the application i have pushed the image into Gitlab Container Registry (`could have used any other free Container Registry eg: DockerHub, GCR, but decided to go with Gitlab's solution.) so that we can use it latter in our deployment.

## Kubernetes/Helm

After having the image ready we need a deployment `tool`. Here there are multiple solutions to use but i chosed the Helm one. (I created the k8s manifests as well)

We reserved a static ip in GCP to use it with our GCE Ingress LB.

As I had a domain lying around I choose to use it for our application.

We added our domain in GCP so that we can manage through it. _You need to wait a couple of hours for the DNS propagation_

Created the A record for our domain (job2me.moffeeshop.com).

Created a Certificate for our domain because HTTPS :D.

In Ingress we defined the annotations (*static_ip*,*frontendconfig*,*paths*,*backends*,*tls*)

## Testing

After the deploying the solution with helm we have this:

```
~   curl job2me.moffeeshop.com
<HTML><HEAD><meta http-equiv="content-type" content="text/html;charset=utf-8">
<TITLE>308 Moved</TITLE></HEAD><BODY>
<H1>308 Moved</H1>
The document has moved
<A HREF="https://job2me.moffeeshop.com/">here</A>.
</BODY></HTML>

```
_I made use of GKE FrontendConfig to redirect all HTTP trafic to HTTPS_

```
~ curl https://job2me.moffeeshop.com
Hello, visitor number 8!

```
Nice!
